<?php
namespace eduluz1976;

/**
 * Class Paginate, responsible for return the queries to fetch
 * items from a list.
 */
class Paginate
{
    public static function buildWhereCondition($username='', $title='')
    {
        $where = "";

        if (empty($username)) {
            $username = '';
        }

        if (empty($title)) {
            $title = '';
        }

        $where = " username > '{$username}' AND title > '{$title}' ";

        return $where;
    }

    public static function getQuery($username='', $title='', $limit=5)
    {
        $query = 'SELECT username, title FROM editcount WHERE ' .
            static::buildWhereCondition($username, $title) .
            " ORDER BY username, title LIMIT $limit;";

        return $query;
    }
}
