<?php

use PHPUnit\Framework\TestCase;

class PaginateTest extends TestCase
{

    /** @test */
    public function test_buildWhereCondition_with_empty_values_should_return_default_initial_where()
    {
        $expected = " username > '' AND title > '' ";
        $actualWhere = eduluz1976\Paginate::buildWhereCondition();

        $this->assertEquals($expected, $actualWhere);
    }


    /** @test */
    public function test_buildWhereCondition_with_valid_values_should_return_expected_where()
    {
        $expected = " username > 'UserB' AND title > 'Bananas' ";
        $actualWhere = eduluz1976\Paginate::buildWhereCondition('UserB', 'Bananas');

        $this->assertEquals($expected, $actualWhere);
    }
}
